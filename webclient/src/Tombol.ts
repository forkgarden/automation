import { BaseComponent } from "./BaseComponent.js";

export class Tombol extends BaseComponent {
	private labelSpan: HTMLSpanElement = null;

	constructor() {
		super();
		this._template = `<button class='tombol white'>
							<span class='label'></span>
						</button>`;
		this.build();
		this.labelSpan = this.getEl('span.label');
	}

	disable():void {
		(this._elHtml as HTMLButtonElement).disabled = true;
	}

	public set onClick(value: Function) {
		this._elHtml.onclick = () => {
			value(this);
		}
	}

	public set label(value: string) {
		this.labelSpan.innerHTML = value;
	}

	public get label(): string {
		return this.labelSpan.innerHTML;
	}

}