import { IStepObj } from "./StepObj.js";

export class TestObj {
	public readonly steps: IStepObj[] = [];
	private _nama: string = '';
	private _id: number = -1;
	
	public get nama(): string {
		return this._nama;
	}
	public set nama(value: string) {
		this._nama = value;
	}

	public get id(): number {
		return this._id;
	}
	public set id(value: number) {
		this._id = value;
	}	
}

export interface ITestObj {
	id: number;
	nama: string;
	readonly steps: IStepObj[];
}

