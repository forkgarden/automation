import { TestObj } from "./Test";

//TODO: merge ke data global
export class LocalSession {
	private _isEditMode: boolean = false;
	private _crTest: TestObj = null;

	public get isEditMode(): boolean {
		return this._isEditMode;
	}
	public set isEditMode(value: boolean) {
		this._isEditMode = value;
	}
	public get crTest(): TestObj {
		return this._crTest;
	}
	public set crTest(value: TestObj) {
		this._crTest = value;
	}
}