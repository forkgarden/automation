import { HomePage } from "./home-page/HomePage.js";
// import { TestObj } from "./Test.js";
import { LocalSession } from "./LocalSession.js";
import { StepObj } from "./Step.js";
import { Dialog } from "./Dialog.js";
import { TestManager } from "./Db.js";

export class App {
	private _testManager: TestManager;
	private _session: LocalSession = new LocalSession();
	private _dialog: Dialog = new Dialog();

	private static _inst: App;

	constructor() {
		App._inst = this;

		window.onload = () => {
			this.init();
		}
	}

	init(): void {
		this._testManager = new TestManager();

		let homePage: HomePage = new HomePage();
		homePage.attach(this.cont);
		homePage.mulai();

		// this._dialog.attach(document.body);
	}

	insertTempTest(): void {
		this._testManager.insert(this.session.crTest);
	}

	public get cont(): HTMLDivElement {
		return document.body.querySelector('div.cont') as HTMLDivElement;
	}

	public static get inst(): App {
		return App._inst;
	}

	public get testManager(): TestManager {
		return this._testManager;
	}

	public get session(): LocalSession {
		return this._session;
	}
	public set session(value: LocalSession) {
		this._session = value;
	}

	public get dialog(): Dialog {
		return this._dialog;
	}

}

export class StepManager {
	//TODO:
	delete(step: StepObj): void {
		step;
	}

}

/*
	step dalam test disimpan dalam bentuk json
*/

