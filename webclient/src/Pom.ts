export class Pom {
	private _name: string = '';
	public get name(): string {
		return this._name;
	}
	public set name(value: string) {
		this._name = value;
	}
	private _css: string = '';
	public get css(): string {
		return this._css;
	}
	public set css(value: string) {
		this._css = value;
	}
	private _xpath: string = '';
	public get xpath(): string {
		return this._xpath;
	}
	public set xpath(value: string) {
		this._xpath = value;
	}

}