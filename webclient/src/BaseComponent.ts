export class BaseComponent {
	protected _template: string = '';
	protected _elHtml: HTMLElement = document.createElement('div');
	// protected _parent: HTMLElement = null;
	// protected _onReturn: Function = null;
	// protected _onCancel: Function = null;

	// onRender(): void {

	// }

	destroy():void {
		if (this.elHtml.parentElement) this.elHtml.parentElement.removeChild(this.elHtml);

		this.destroyRec(this.elHtml);
	}

	destroyRec(el:Element):void {
		while (el.firstElementChild) {
			let el2:Element = el.removeChild(el.firstElementChild);
			if (el2) {
				this.destroyRec(el2);
			}
		}
	}

	mulai():void {

	}

	onBuild(): void {

	}

	onAttach(): void {

	}

	onDetach(): void {

	}

	onShow(): void {

	}

	onHide(): void {

	}

	attach(parent: HTMLElement): void {
		parent.appendChild(this._elHtml);
		// this._parent = parent;
		this.onAttach();
	}

	detach(): void {
		if (this._elHtml.parentElement) {
			this._elHtml.parentElement.removeChild(this._elHtml);
		}
		this.onDetach();
	}

	show(el?: HTMLElement): void {
		if (!el) {
			el = this._elHtml;
		}

		el.style.display = 'block';
		this.onShow();
	}

	hide(el?: HTMLElement): void {
		if (!el) {
			el = this._elHtml;
		}

		el.style.display = 'none';
		this.onHide();
	}


	build(): void {
		let div: HTMLElement = document.createElement('div');
		let el: HTMLElement;

		div.innerHTML = this._template;

		el = div.firstElementChild as HTMLElement;

		if (el == null) {
			console.log(this._template);
			throw new Error('failed to build el');
		}

		if (div.childElementCount == 0) {
			console.error('should be wrapped as singgle element')
		}

		this._elHtml = el;

		this.onBuild();
	}

	getEl(query: string): HTMLElement {
		let el: HTMLElement;

		el = this._elHtml.querySelector(query);

		if (el) {
			return el
		} else {
			console.log(this._elHtml);
			console.log(query);
			throw new Error('query not found ');
		}
	}

	// render(parent: HTMLElement): void {
		// this.build();
		// this.attach(parent);
		// this._parent = parent;
		// this.onRender();
	// }

	public get template() {
		return this._template;
	}

	public set template(str: string) {
		this._template = str;
	}

	public get elHtml(): HTMLElement {
		return this._elHtml;
	}
	public set elHtml(value: HTMLElement) {
		this._elHtml = value;
	}

	// public set onReturn(value: Function) {
		// console.log(this);
		// console.log('set on return');
		// console.log(value);
		// this._onReturn = value;
	// }
	
	// public set onCancel(value: Function) {
	// 	this._onCancel = value;
	// }
}
