import { BaseComponent } from "../BaseComponent.js";
import { StepType } from "../Enum.js";
import { Menu } from "../menu-page/Menu.js";
import { App } from "../App.js";
import { StepObj } from "../Step.js";
import { TestPage } from "../test-page/TestPage.js";

export class StepParam extends BaseComponent {
	private elmCont: HTMLDivElement = null;
	private okTbl: HTMLButtonElement = null;
	private paramList: Array<IParam> = null;

	constructor() {
		super();
		this._template = `
			<div class='step-param'>
				<div class='close-btn-cont'>
					<button class='close'>X</button>
				</div>
				<p>Step Parameter:</p>
				<div class='elm-cont'></div>
				<button class='ok'>OK</button>
			</div>
		`;
		this.build();

		this.elmCont = this.getEl('div.elm-cont') as HTMLDivElement;
		this.closeTbl.onclick = () => {
			this.detach();
			let testPage: TestPage = new TestPage();
			testPage.attach(App.inst.cont);
			testPage.mulai();
		}

		this.okTbl = this.getEl('button.ok') as HTMLButtonElement;
		this.okTbl.onclick = () => {
			this.okTblOnClick();
		}

		this.paramList = []
	}

	mulai(): void {
		let type: string = App.inst.session.crTest.tempStep.cmd;

		if (StepType.BROWSER_SWITCH == type) {
			this.tambahInputElement("Browser index to switch to:", "number");
		}
		else if (StepType.CLICK == type) {
			this.tambahSelectElement();
		}
		else if (StepType.COMBINE == type) {
			//TODO: select other test element
		}
		else if (StepType.COMMENT == type) {
			this.tambahInputElement('Comment:');
		}
		else if (StepType.DELAY == type) {
			this.tambahInputElement('Delay in millisec:', "number");
		}
		else if (StepType.DELAY_MIN == type) {
			this.tambahInputElement('Delay in minute:', "number");
		}
		else if (StepType.DELAY_SEC == type) {
			this.tambahInputElement('Delay in seconds:', "number");
		}
		else if (StepType.ERROR == type) {
			this.okTblOnClick();
		}
		else if (StepType.MAXIMIZE == type) {
			this.okTblOnClick();
		}
		else if (StepType.NAVIGATE == type) {
			this.tambahInputElement("Url to navigate:");
		}
		else if (StepType.OPEN_BROWSER == type) {
			this.tambahRadioElement("Select browser type", ["Chrome", "Firefox"]);
		}
		else if (StepType.QUIT == type) {
			this.okTblOnClick();
		}
		else if (StepType.RUNJS == type) {
			//TODO: dialog js
		}
		else if (StepType.RUNOTHERTEST == type) {
			//TODO: double
		}
		else if (StepType.SEND_KEY == type) {
			this.tambahSelectElement();
		}
		else if (StepType.SWTICH_TO_FRAME == type) {
			this.tambahInputElement('Frame idx to switch to:', 'number');
		}
		else if (StepType.VALIDATE_ELEMENT_ENABLE == type) {
			this.tambahSelectElement();
		}
		else if (StepType.VALIDATE_ELEMENT_LOCATED == type) {
			this.tambahSelectElement();
		}
		else if (StepType.VALIDATE_ELEMENT_VISIBLE == type) {
			this.tambahSelectElement();
		}
		else if (StepType.WRITE == type) {
			this.tambahSelectElement();
		}
		else {
			throw new Error('type undefined ' + type);
		}
	}

	okTblOnClick(): void {

		console.log('step param on ok click ' + App.inst.session.crTest.tempStep);

		for (let i: number = 0; i < this.paramList.length; i++) {
			this.paramList[i].update(App.inst.session.crTest.tempStep);
		}

		//TODO: bawa ke test => commit step
		App.inst.session.crTest.addStep(App.inst.session.crTest.tempStep);
		App.inst.session.crTest.tempStep = null;

		this.detach();
		let testPage = new TestPage();
		testPage.attach(App.inst.cont);
		testPage.mulai();
	}

	tambahSelectElement(): void {
		let selectElm: SelectElementFragment = new SelectElementFragment();
		selectElm.stepParam = this;
		selectElm.attach(this.elmCont);
		this.paramList.push(selectElm);

		selectElm.elmTbl.onclick = () => {
			let menu: Menu = new Menu();

			console.log("elm tbl on click");
			this.detach();
			menu.attach(App.inst.cont);
		}
	}

	tambahInputElement(label: string, type = "text"): void {
		let input: InputTextFragment = new InputTextFragment();
		input.labelEl.innerText = label;
		input.inputEl.type = type;
		input.attach(this.elmCont);
		this.paramList.push(input);
	}

	tambahRadioElement(judul: string, labels: string[]): void {
		let radio: RadioFragment = new RadioFragment();
		radio.judulP.innerText = judul;
		radio.attach(this.elmCont);
		this.paramList.push(radio);

		labels.forEach((item: string) => {
			radio.buatRadio(item);
		})
	}

	public get closeTbl(): HTMLButtonElement {
		return this.getEl('button.close') as HTMLButtonElement;
	}

}

interface IParam {
	update(step: StepObj): void;
}

class SelectElementFragment extends BaseComponent implements IParam {
	private _stepParam: StepParam = null;
	private _step: StepObj;

	constructor() {
		super();
		this._template = `
			<div class='select-element'> 
				<label>Select Element:</label><br/> 
				<input type='text' readonly></input>
				<button class='elm'>...</button>
			</div>
		`;
		this.build();
	}

	//TODO:
	update(step: StepObj): void {
		step;
		// step.el = this.menu.selectedChild.pom;
	}

	//TODO: goto parent
	// menuOkClick(): void {
	// 	this.menu.detach();
	// 	this.stepParam.attach(App.inst.cont);
	// 	this.inputEl.value = this.menu.selectedChild.label;
	// }

	// public get pom(): PomStructure {
	// 	return this._pom;
	// }

	public get stepParam(): StepParam {
		return this._stepParam;
	}
	public set stepParam(value: StepParam) {
		this._stepParam = value;
	}
	public get step(): StepObj {
		return this._step;
	}
	public set step(value: StepObj) {
		this._step = value;
	}
	public get inputEl(): HTMLInputElement {
		return this.getEl('input') as HTMLInputElement;
	}
	public get elmTbl(): HTMLButtonElement {
		return this.getEl('button.elm') as HTMLButtonElement;
	}


}

class InputTextFragment extends BaseComponent implements IParam {
	constructor() {
		super();
		this._template = `
			<div class='input-text'>
				<label class='label'></label><br/>
				<input type='text' class='input'></input>
			</div>
		`;
		this.build();
	}

	update(step: StepObj): void {
		step.data.push(this.inputEl.value);
	}

	public get labelEl(): HTMLLabelElement {
		return this.getEl('label.label') as HTMLLabelElement;
	}

	public get inputEl(): HTMLInputElement {
		return this.getEl('input.input') as HTMLInputElement;
	}
}

class RadioFragment extends BaseComponent implements IParam {
	private value: string = '';

	constructor() {
		super();

		this._template = `
			<div>
				<p></p>
				<div class='radio-cont'>
				</div>
			</div>
		`;
		this.build();
	}

	get judulP(): HTMLParagraphElement {
		return this.getEl('p') as HTMLParagraphElement;
	}

	//TODO: label bisa di click
	buatRadio(label: string): void {
		let radio: BaseComponent = null;
		let input: HTMLInputElement = null;
		// let value:string = '';

		radio = new BaseComponent();
		radio.template = `<div><input type="radio" name="radio" checked value="${label}" id="${label}"/><label for="${label}"></label><br/></div>`;
		radio.build();
		radio.getEl('label').innerText = label;
		radio.attach(this.cont);

		input = radio.getEl("input[type=radio]") as HTMLInputElement;
		input.onclick = () => {
			this.value = input.value;
		}
		this.value = input.value;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.radio-cont') as HTMLDivElement;
	}

	update(step: StepObj): void {
		//get selected radio
		this.elHtml.querySelectorAll('input[type=radio]') //TODO:
		step.data.push(this.value);
	}

}