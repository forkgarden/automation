import { StepObj } from "./Step";

export class TestObj implements ITestObj {
	private _steps: Array<StepObj> = [];
	private _nama: string = '';
	private _tempStep: StepObj = null;
	private _id: number = -1;
	private _steps2: number[];	//TODO: kemungkinan depcreated

	public get steps2(): number[] {
		return this._steps2;
	}
	public set steps2(value: number[]) {
		this._steps2 = value;
	}

	addStep(step: StepObj): void {
		this.steps.push(step);
	}

	public get nama(): string {
		return this._nama;
	}
	public set nama(value: string) {
		this._nama = value;
	}

	public get tempStep(): StepObj {
		return this._tempStep;
	}
	public set tempStep(value: StepObj) {
		this._tempStep = value;
	}

	public get id(): number {
		return this._id;
	}
	public set id(value: number) {
		this._id = value;
	}
	public get steps(): Array<StepObj> {
		return this._steps;
	}
}

export interface ITestObj {
	steps: Array<StepObj>;
	nama: string;
	tempStep: StepObj;
	id: number;
	steps2: number[];	//TODO: kemungkinan depcreated	
}