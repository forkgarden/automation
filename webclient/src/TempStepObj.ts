import { IStepObj } from "./StepObj.js";
import { ITestObj } from "./TestObj.js";

export class TempStepObj {
    private _step: IStepObj = null;
    private _test: ITestObj = null;
    private _isNew: boolean = false;
    
    public get isNew(): boolean {
        return this._isNew;
    }
    public set isNew(value: boolean) {
        this._isNew = value;
    }

    public get step(): IStepObj {
        return this._step;
    }
    public set step(value: IStepObj) {
        this._step = value;
    }
    public get test(): ITestObj {
        return this._test;
    }
    public set test(value: ITestObj) {
        this._test = value;
    }
}