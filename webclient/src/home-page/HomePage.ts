import { BaseComponent } from "../BaseComponent.js";
import { Tombol } from "../Tombol.js";
import { TestItemView } from "./TestView.js";
import { App } from "../App.js";
import { TanyaNamaTest } from "./TestTanyaNama.js";
import { TestPage } from "../test-page/TestPage.js";
import { TestObj } from "../Test.js";
import { TestManager } from "../Db.js";

export class HomePage extends BaseComponent {
	protected tblNew: Tombol = null;
	protected tblEdit: Tombol = null;
	protected tblDelete: Tombol = null;
	protected tblRun: Tombol = null;
	protected btnCont: HTMLDivElement = null;
	protected editMode: boolean = false;

	protected readonly list: Array<TestItemView> = [];

	constructor() {
		super();
		this._template = `
			<div class='home-page'>
				<h1>Automation Designer</h1>
				<p class='description'></p>
				<div class='cont'>
					<div class='list list-cont'></div>
					<div class='button'></div>
				</div>
			</div>
		`;
		this.build();

		this.btnCont = this.getEl('div.button') as HTMLDivElement;

		this.tblNew = new Tombol();
		this.tblNew.label = 'New';
		this.tblNew.attach(this.btnCont);
		this.tblNew.onClick = () => {
			this.tblNewClick();
		}

		this.tblEdit = new Tombol();
		this.tblEdit.label = 'Edit';
		this.tblEdit.attach(this.btnCont);
		this.tblEdit.onClick = () => {
			this.tblEditClick();
		}

		this.tblDelete = new Tombol();
		this.tblDelete.label = 'Delete';
		this.tblDelete.attach(this.btnCont);
		// this.tblDelete.disable();
		this.tblDelete.onClick = () => {
			this.tblDeleteClick();
		}

		this.tblRun = new Tombol();
		this.tblRun.label = 'Run';
		this.tblRun.attach(this.btnCont);
		// this.tblRun.disable();
		this.tblRun.onClick = () => {
			this.tblRunClick();
		}

		// this.listCont = this.getEl('div.list') as HTMLDivElement;
		// this.testPage = new TestPage();
	}

	mulai(): void {
		let testManager: TestManager = App.inst.testManager;
		let list: Array<TestObj> = testManager.tests;

		console.log("render test");

		for (let i: number = 0; i < list.length; i++) {
			let test: TestObj = list[i];
			let view: TestItemView = new TestItemView();

			view.test = test;
			view.attach(this.listCont);
			view.elHtml.onclick = () => {
				console.log('test selected ' + view.test.nama);

				//deselect all
				for (let i: number = 0; i < this.list.length; i++) {
					this.list[i].elHtml.classList.remove('selected');
				}

				//TODO: view test item selected
				view.elHtml.classList.add('selected');
				App.inst.session.crTest = view.test;


			}
			this.list.push(view);
		}

		//set default selected
		if (list.length > 0) {
			App.inst.session.crTest = list[0];
			this.list[0].elHtml.classList.add('selected')
		}
		else {
			App.inst.session.crTest = null;
		}

		//update tombol
	}

	updateTombolState(): void {

	}

	tblRunClick(): void {
		if (!App.inst.session.crTest) {
			console.warn('No test selected');
			return;
		}

		console.log('send data');

		// console.log(App.inst.testManager.toJsonData());
		// var data: FormData = new FormData();
		// data.append('test', App.inst.testManager.toJsonData());
		// data.append("test2", "[{\"steps\":[{\"_cmd\":1,\"_el\":null,\"_comment\":\"\",\"_data\":[]}],\"_nama\":\"test\"}]");

		var xhr: XMLHttpRequest = new XMLHttpRequest();
		xhr.open('POST', 'http://localhost:3009/json', true);
		xhr.setRequestHeader('Content-type', 'application/json');

		xhr.onreadystatechange = (ev: Event) => {
			ev;
			// console.log(ev);
			// console.log(xhr.status);
			// console.log(xhr.statusText);
			// console.log("==============");
		}

		xhr.onload = function () {

			if (xhr.status !== 200) {
				console.log('xhr ' + xhr.status);
				console.log('xhr ' + xhr.statusText);
				return; // return is important because the code below is NOT executed if the response is other than HTTP 200 (OK)
			}

			console.log('response');
			console.log(xhr.responseText);
		};

		//TODO: provide data
		let data: string = JSON.stringify(App.inst.session.crTest);
		console.log(data);
		xhr.send(data);
		//xhr.send("[{\"steps\":[{\"_cmd\":1,\"_el\":null,\"_comment\":\"\",\"_data\":[]}],\"_nama\":\"test\"}]");
	}

	tblDeleteClick(): void {
		if (!App.inst.session.crTest) {
			console.warn('No test selected');
			return;
		}

	}

	tblEditClick(): void {
		console.log('edit click');
		if (!App.inst.session.crTest) {
			console.warn('No test selected');
			return;
		}

		let testPage: TestPage = new TestPage();

		// testPage.editMode = true;
		this.detach();
		testPage.attach(App.inst.cont);
	}

	tblNewClick(): void {
		console.log(this + ' tblNewClick');
		App.inst.session.crTest = new TestObj();

		this.detach();
		let tanyaNama = new TanyaNamaTest();
		tanyaNama.attach(App.inst.cont);
		tanyaNama.okClick = (nama: string) => {
			console.log(this + ' tanyaNamaOkClick, nama ' + nama);

			App.inst.session.crTest.nama = nama;
			let testPage = new TestPage();
			testPage = new TestPage();
			testPage.attach(App.inst.cont);
		}

		tanyaNama.closeClick = () => {
			this.attach(App.inst.cont);
		}
	}

	get listCont(): HTMLDivElement {
		return this.getEl('div.cont div.list') as HTMLDivElement;
	}
}