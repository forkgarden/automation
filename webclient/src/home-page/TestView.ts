import { BaseComponent } from "../BaseComponent.js";
import { TestObj } from "../Test.js";

export class TestItemView extends BaseComponent {
	private _test: TestObj;

	constructor() {
		super();
		this._template = `
			<div class='test-item list-item'>
				<p class='nama'></p>
				<p class='description'></p>
			</div>
		`;
		this.build();
	}

	public get namaP(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}

	get desc(): HTMLParagraphElement {
		return this.getEl('p.description') as HTMLParagraphElement;
	}

	public set test(value: TestObj) {
		this._test = value;
		this.namaP.innerText = this._test.nama;
	}
	public get test(): TestObj {
		return this._test;
	}


}