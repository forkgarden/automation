import { BaseComponent } from "../BaseComponent.js";

export class TanyaNamaTest extends BaseComponent {
	private inputTxt: HTMLInputElement = null;
	private okTbl: HTMLButtonElement = null;
	private tutupTbl: HTMLButtonElement = null;

	private _okClick: Function;

	constructor() {
		super();

		this._template = `
			<div class='tanya-nama'>
				<div class='cont'>
					<div class='close-btn-cont'>
						<button class='close'>X</button><br/>
					</div>
					<label>Name:</label><br/>
					<input type='text' name='nama' class='nama'></input><br/>
					<p>
						<button class='ok'>OK</button>
					</p>
				</div>
			</div>
		`;

		this.build();
		this.inputTxt = this.getEl('input.nama') as HTMLInputElement;
		this.okTbl = this.getEl('button.ok') as HTMLButtonElement;
		this.tutupTbl = this.getEl('button.close') as HTMLButtonElement;

		this.okTbl.onclick = () => {
			this._okClick();
		}

		//TODO: debug
		this.inputTxt.value = 'test 1';

		console.log('TestNama:constructor');
	}

	set closeClick(f: Function) {
		this.tutupTbl.onclick = () => {
			this.detach();
			f();
		}
	}

	public set okClick(value: Function) {
		this._okClick = value;

		this.okTbl.onclick = () => {
			this.detach();
			this._okClick(this.inputTxt.value);
		}
	}

}