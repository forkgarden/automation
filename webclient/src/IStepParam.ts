import { PomStructure } from "./PomStructure";

export interface IStepParam {
    command:number;
    pom?:PomStructure;
    data?:Array<any>;
}