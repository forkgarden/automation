import { ITombol} from "./Menu.js";

export enum EnumElType {
	LINK,
	INPUT_TEXT,
	BUTTON,
	SELECT,
	TABLE_ROW,
	TEXT_AREA
}

const agentPage: ITombol = {
    label: "Agent",
    members: [
        {
            label: 'Context Menu',
            members: [
                {
                    label: "Identify Customer",
                    type: EnumElType.LINK,
                    pom: { xpath: "//a[contains(.,'Identify Customer')][1]" }
                },
                {
                    label: "Warp Up",
                    type: EnumElType.BUTTON,
                    pom: { css: "button[id$=btnWrapup]" }
                }
            ]
        },
        {
            label: "Identify Customer",
            members: [
                {
                    label: "first name",
                    type: EnumElType.INPUT_TEXT,
                    pom: { css: "input[id$=txtFirstName][type=text][name$=txtFirstName]" }
                },
                {
                    label: "last name",
                    type: EnumElType.INPUT_TEXT,
                    pom: { css: "input[id$=txtLastName].GTTextField.txtLastName[type=text]" }
                },
                {
                    label: "search",
                    type: EnumElType.BUTTON,
                    pom: { css: "button[id$=btnSearch][name=gtxReserved_noJavaScript_actionString].text-in-center" }
                },
                {
                    label: "search result",
                    type: EnumElType.TABLE_ROW,
                    members: [
                        {
                            label: "first row",
                            type: EnumElType.TABLE_ROW,
                            pom: { css: 'table tbody.yui-dt-data tr:nth-child(1) td' },
                        },
                        {
                            label: "second row",
                            type: EnumElType.TABLE_ROW,
                            pom: { css: 'table tbody.yui-dt-data tr:nth-child(2) td' },
                        },
                        {
                            label: "third row",
                            type: EnumElType.TABLE_ROW,
                            pom: { css: 'table tbody.yui-dt-data tr:nth-child(3) td' },
                        }

                    ]
                },
                {
                    label: "select",
                    type: EnumElType.BUTTON,
                    pom: { css: "button[id$=btnSelect][name=gtxReserved_noJavaScript_actionString]" }
                },
            ]
        },
        {
            label: "WarpUp",
            members: [
                {
                    label: "reason code",
                    members: [
                        {
                            label: "complete",
                            type: EnumElType.SELECT,
                            pom: { css: "select[id$=optReasonCodes].GTOptionMenu[name$=optReasonCodes] option[value='Completed']" }
                        }
                    ]
                },
                {
                    label: 'note',
                    type: EnumElType.TEXT_AREA,
                    pom: { css: "div.notes-textcontainter textarea.note-input" }
                },
                {
                    label: 'confirm',
                    type: EnumElType.BUTTON,
                    pom: { css: "button[id$=btnConfirm]" }
                }
            ]
        }
    ]
}

const logoutPage: ITombol = {
    label: 'logout',
    members: [
        {
            label: 'relogin',
            type: EnumElType.BUTTON,
            pom: { xpath: "//button[contains(text(), 'Login again')][1]" }

        }
    ]
}

const loginPage: ITombol = {
    label: 'login',
    members: [
        {
            label: 'user name',
            type: EnumElType.INPUT_TEXT,
            pom: { css: "div input[type=text]" }
        },
        {
            label: 'password',
            type: EnumElType.INPUT_TEXT,
            pom: { css: "div input[type=password]" }
        },
        {
            label: 'submit',
            type: EnumElType.BUTTON,
            pom: { xpath: "//button[@type='submit'][1]" }

        }
    ]
}

const Url: ITombol = {
    label: 'URLS',
    members: [
        {
            label: 'ad',
            type: EnumElType.BUTTON,
            link: "http://localhost:8280/GTConnect/UnifiedAcceptor/FrameworkDesktop.Main"
        }
    ]
}

export const page: ITombol = {
    label: 'page',
    members: [
        {
            label: 'homepage',
            members: [
                {
                    label: 'logout',
                    type: EnumElType.BUTTON,
                    pom: { css: "button[id$=btnLogout].text-in-center[name=gtxReserved_noJavaScript_actionString]" }
                }
            ]
        },
        {
            label: 'logout',
            members: [
                {
                    label: 'confirm dialog',
                    members: [
                        {
                            label: 'confirm',
                            type: EnumElType.BUTTON,
                            pom: { css: "button[id$=btnConfirm]" }
                        }
                    ]
                }
            ]
        },
        agentPage,
        logoutPage,
        loginPage,
        Url
    ]
}