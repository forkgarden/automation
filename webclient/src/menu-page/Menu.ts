import { PomStructure } from "../PomStructure.js";
import { BaseComponent } from "../BaseComponent.js";
import { TombolMenu } from "./Tombol.js";
import { page } from "./Data.js";

export class Menu extends BaseComponent {
	private _list: Array<TombolMenu> = [];
	private _insertPlace: number = 0;
	private btnAbove: HTMLButtonElement = null;
	private btnBelow: HTMLButtonElement = null;
	private _selectedChild: ITombol;
	private _click: Function = null;
	private cont:HTMLDivElement = null;

	static readonly INSERT_BELOW: number = 1;
	static readonly INSERT_ABOVE: number = 2;

	constructor() {
		super();
		this._template = `
			<div class='menu'>
				<div class='cont'></div>
				<button class='insert-above'>Insert Above</button>
				<button class='insert-below'>Insert Below</button>
			</div>
		`;

		this.build();
		this.btnAbove = this.getEl('button.insert-above') as HTMLButtonElement;
		this.btnBelow = this.getEl('button.insert-below') as HTMLButtonElement;
		this.cont = this.getEl('div.cont') as HTMLDivElement;

		this.btnAbove.onclick = () => {
			this._insertPlace = Menu.INSERT_ABOVE;
			this._click();
		}

		this.btnBelow.onclick = () => {
			this._insertPlace = Menu.INSERT_BELOW;
			this._click();
		}

		this.cont.appendChild(this.renderMenu(page));
	}

	mulai():void {
		
	}

	renderMenu(data: ITombol): HTMLDivElement {
		let rootDiv: HTMLDivElement = document.createElement('div');
		let rootUl: HTMLUListElement = document.createElement('ul');
		rootDiv.appendChild(rootUl);
		rootDiv.classList.add('menu-ul');

		if (data.members) this.renderChild(data.members, rootUl);
		return rootDiv;
	}

	renderChild(childs: Array<ITombol>, ulParent: HTMLUListElement): void {
		for (let i: number = 0; i < childs.length; i++) {
			let li: HTMLLIElement = document.createElement('li');
			let child: ITombol = childs[i];

			if (child.members && child.members.length > 0) {
				let text: Text = null;
				let tbl: HTMLButtonElement = document.createElement('button');

				text = document.createTextNode(child.label);
				tbl.appendChild(text);
				li.appendChild(tbl);

				let ul: HTMLUListElement = document.createElement('ul');
				ul.style.display = 'none';
				li.appendChild(ul);
				li.classList.add('label');

				li.onclick = (e: MouseEvent) => {
					e.stopPropagation();
					if (ul.style.display == 'none') {
						ul.style.display = 'block';
					}
					else {
						ul.style.display = 'none';
					}
				}

				if (child.members) this.renderChild(child.members, ul);
			}
			else {
				let tombol: TombolMenu = new TombolMenu();
				tombol.label = child.label;

				if (child.description) {
					tombol.desc = child.description;
				}

				tombol.onClick = () => {
					// console.log('child on click');
					// console.log(tombol.label);
					this._selectedChild = child;
				}

				tombol.attach(li);
			}

			ulParent.appendChild(li);
		}
	}

	public get list(): Array<TombolMenu> {
		return this._list;
	}

	public get insertPlace(): number {
		return this._insertPlace;
	}
	public set insertPlace(value: number) {
		this._insertPlace = value;
	}

	public get click(): Function {
		return this._click;
	}
	public set click(value: Function) {
		this._click = value;
	}
	public get selectedChild(): ITombol {
		return this._selectedChild;
	}
	public set selectedChild(value: ITombol) {
		this._selectedChild = value;
	}
}

export interface ITombol {
	label: string;
	type?: number,
	pom?: PomStructure;
	onclick?: Function;
	description?: string;	
	members?: Array<ITombol>;
	idx?: number;	
	link?: string;
}