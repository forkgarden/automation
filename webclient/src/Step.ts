// import { Pom } from "./Pom";
import { PomStructure } from "./PomStructure";

export class StepObj implements IStepObj {
	private _cmd: string = '';
	private _el: PomStructure = null;
	private _comment: string = '';
	private _data: Array<any> = [];

	get elName(): string {
		if (!this._el) return "no-name";
		if (this._el.name) return this._el.name;
		if (this._el.css) return this._el.css;
		if (this._el.xpath) return this._el.xpath;
		return "no-name";
	}

	public get cmd(): string {
		return this._cmd;
	}
	public set cmd(value: string) {
		this._cmd = value;
	}
	public get comment(): string {
		return this._comment;
	}
	public set comment(value: string) {
		this._comment = value;
	}
	public get data(): Array<any> {
		return this._data;
	}
	public set data(value: Array<any>) {
		this._data = value;
	}
	public get el(): PomStructure {
		return this._el;
	}
	public set el(value: PomStructure) {
		if (!value) throw new Error();
		this._el = value;
	}

}

export interface IStepObj {
	cmd: string;
	el: PomStructure;
	comment: string;
	data: Array<any>;
}