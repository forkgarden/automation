import { BaseComponent } from "./BaseComponent.js";

export class Dialog extends BaseComponent {
    constructor() {
        super();
        this._template = `
            <div class='dialog'>
                <div class='box'>
                    <br/>
                    <div class='content'>
                        message here
                    </div>
                    <div class='footer'>
                        <button class='ok'>OK</button>
                        <br/>
                        <br/>
                    </div>
                </div>
            </div>
        `;
        this.build();
    }

    get content():HTMLDivElement {
        return this.getEl('div.content') as HTMLDivElement;
    }

    get okTbl():HTMLButtonElement {
        return this.getEl('button.ok') as HTMLButtonElement;
    }
}