import { StepObj } from "./StepObj.js";

export class SessionData {
	private _tempStep: StepObj = null;
	private _crStepId: number = 0;

	public get crStepId(): number {
		return this._crStepId;
	}
	public set crStepId(value: number) {
		this._crStepId = value;
	}

	public get tempStep(): StepObj {
		return this._tempStep;
	}
	public set tempStep(value: StepObj) {
		this._tempStep = value;
	}
}

export enum EditMode {
	BARU,
	EDIT
}