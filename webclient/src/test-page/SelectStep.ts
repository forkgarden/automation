import { BaseComponent } from "../BaseComponent.js";
import { StepType } from "../Enum.js";
import { StepParam } from "../step-param/StepParam.js";
import { App } from "../App.js";
import { StepObj } from "../Step.js";
import { TestPage } from "./TestPage.js";

export class SelectStep extends BaseComponent {

	protected cont: HTMLDivElement = null;
	protected nextTbl: HTMLButtonElement;
	// private _tutupTbl: HTMLButtonElement;
	protected selectedItemView: StepItemView;
	protected listView: Array<StepItemView> = [];
	protected stepParam: StepParam = null;

	constructor() {
		super();

		this._template = `
			<div class='select-step'>
				<div class='close-btn-cont'>
					<button class='close'>X</button>
				</div>
				<p class='judul'>Select Step:</p>
				<div class='list-cont'></div>
				<br/>
				<button class='ok'>Next</button>
			</div>
		`;

		this.build();
		this.nextTbl = this.getEl('button.ok') as HTMLButtonElement;
		// this.tutupTbl = 
		this.cont = this.getEl('div.list-cont') as HTMLDivElement;

		this.nextTbl.style.display = 'none';
		this.nextTbl.onclick = () => {
			this.nextTblOnClick();
		}

		this.tutupTbl.onclick = () => {
			let testPage: TestPage = new TestPage();

			this.detach();
			testPage.attach(App.inst.cont);
		}
	}

	mulai(): void {
		this.renderStep();
	}

	nextTblOnClick(): void {
		this.detach();

		this.stepParam = new StepParam();
		this.stepParam.attach(App.inst.cont);
		this.stepParam.mulai();
	}

	renderStep(): void {
		for (let i: number = 0; i < stepTypeListDesc.length; i++) {
			let type: StepStructure = stepTypeListDesc[i];
			let view: StepItemView = null;

			view = new StepItemView();
			view.desc = type.desc;
			view.nama = type.nama;
			view.type = type.type;
			view.elHtml.onclick = () => {
				this.onItemViewClick(view);
			}

			view.attach(this.cont);
			this.listView.push(view);
		}
	}

	onItemViewClick(view: StepItemView): void {
		this.selectedItemView = view;
		for (let i: number = 0; i < this.listView.length; i++) {
			let item: StepItemView = this.listView[i]
			item.deselect();
			if (item === view) {
				item.select();
			}
		}
		App.inst.session.crTest.tempStep.cmd = view.type;
		this.nextTbl.style.display = 'block';
		console.log('onclick step type: ' + view.type + '/cmd type ' + App.inst.session.crTest.tempStep.cmd);
	}

	protected get tutupTbl(): HTMLButtonElement {
		return this.getEl('button.close') as HTMLButtonElement;
	}


}

class StepItemView extends BaseComponent {
	protected namaP: HTMLParagraphElement = null;
	protected descP: HTMLParagraphElement = null;
	protected _type: string = "";
	protected _step: StepObj = null;
	public get step(): StepObj {
		return this._step;
	}
	public set step(value: StepObj) {
		this._step = value;
	}
	// protected step: Step;

	public get type(): string {
		return this._type;
	}
	public set type(value: string) {
		this._type = value;
	}

	select(): void {
		this.deselect();
		this.elHtml.classList.add('selected');
	}

	deselect(): void {
		this.elHtml.classList.remove('selected');
	}

	constructor() {
		super();
		this._template = `
			<div class='step-type'>
				<p class='nama'></p>
				<p class='desc'></p>
			</div>
		`;
		this.build();

		this.namaP = this.getEl('p.nama') as HTMLParagraphElement;
		this.descP = this.getEl('p.desc') as HTMLParagraphElement;

	}

	public get nama(): string {
		return this.namaP.innerText;
	}
	public set nama(value: string) {
		this.namaP.innerHTML = value;
	}

	public get desc(): string {
		return this.descP.innerText;
	}
	public set desc(value: string) {
		this.descP.innerHTML = value;
	}

}

export interface StepStructure {
	type: string,
	nama: string,
	desc: string,
	code?: string
}

export const stepTypeListDesc: Array<StepStructure> = [
	{
		type: StepType.BROWSER_SWITCH,
		nama: 'switch browser',
		desc: "Change browser"
	},
	{
		type: StepType.OPEN_BROWSER,
		nama: 'open browser',
		desc: 'open browser'
	},
	{
		type: StepType.QUIT,
		nama: 'quit',
		desc: 'quit'
	},
	{
		type: StepType.CLICK,
		nama: 'click',
		desc: "Click a button"
	},
	{
		type: StepType.COMBINE,
		nama: 'combine',
		desc: "Combine a test"
	},
	{
		type: StepType.COMMENT,
		nama: 'comment',
		desc: ""
	},
	{
		type: StepType.DELAY,
		nama: 'delay',
		desc: ""
	}

];