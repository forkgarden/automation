import { BaseComponent } from "../BaseComponent.js";
import { Tombol } from "../Tombol.js";
import { SelectStep, StepStructure } from "./SelectStep.js";
import { App } from "../App.js";
import { StepObj } from "../Step.js";
import { TestObj } from "../Test.js";
import { stepTypeListDesc } from "./SelectStep.js"
import { HomePage } from "../home-page/HomePage.js";
import { StepType } from "../Enum.js";

export class TestPage extends BaseComponent {
	private addTbl: Tombol = null;
	private deleteTbl: Tombol = null;
	private editTbl: Tombol = null;
	private okTbl: Tombol = null;
	private selectStep: SelectStep = null;
	private tblCont: HTMLDivElement = null;
	private listCont: HTMLDivElement = null;
	private _judul: HTMLSpanElement = null;
	private crStep: StepObj = null;
	private readonly viewList: Array<StepItemView> = [];

	constructor() {
		super();
		this._template = `
			<div class='test-page'>
				<div class='close-btn-cont'>
					<button class='close'>X</button>
				</div>

				<p class='judul'>Edit Test <span class='test-name'></span></p>
				
				<div class='cont'> 
					<div class='step list-cont'></div>
					<div class='btn-cont'></div>
				</div>
			</div>
		`;
		this.build();

		this._judul = this.getEl('span.test-name') as HTMLSpanElement;
		this.tblCont = this.getEl('div.btn-cont') as HTMLDivElement;
		this.listCont = this.getEl('div.list-cont') as HTMLDivElement;

		this.addTbl = new Tombol();
		this.addTbl.label = 'add';
		this.addTbl.attach(this.tblCont);
		this.addTbl.onClick = () => {
			this.addClick();
		}

		this.deleteTbl = new Tombol();
		this.deleteTbl.label = 'delete';
		this.deleteTbl.attach(this.tblCont);
		this.deleteTbl.onClick = () => {
			this.deleteClick();
		}

		this.editTbl = new Tombol();
		this.editTbl.label = 'edit';
		this.editTbl.attach(this.tblCont);
		this.editTbl.onClick = () => {
			this.editClick();
		}

		this.okTbl = new Tombol();
		this.okTbl.label = 'Save & Close';
		this.okTbl.attach(this.tblCont);
		this.okTbl.onClick = () => {
			this.detach();

			if (App.inst.session.isEditMode) {
				App.inst.testManager.update(App.inst.session.crTest.id, App.inst.session.crTest);
			}
			else {
				App.inst.testManager.insert(App.inst.session.crTest);
			}

			let homePage: HomePage = new HomePage();
			homePage.attach(App.inst.cont);
			homePage.mulai();
		}

		this.closeTbl.onclick = () => {
			let homePage: HomePage;

			this.detach();
			homePage = new HomePage();
			homePage.attach(App.inst.cont);
			homePage.mulai();
		}
	}

	mulai(): void {
		this._judul.innerText = App.inst.session.crTest.nama;
		this.renderStep();
	}

	deselectAll(): void {
		this.viewList.forEach((item: StepItemView) => {
			item.deselect();
		})
	}

	renderStep(): void {
		let test: TestObj = App.inst.session.crTest;
		let list: Array<StepObj> = test.steps;

		console.log('TestPage: render step, list length ' + list.length);
		console.group("list");

		while (this.listCont.firstChild) {
			this.listCont.removeChild(this.listCont.firstChild);
		}

		for (let i: number = 0; i < list.length; i++) {
			let step: StepObj = list[i];
			let view: StepItem = new StepItem();
			view.step = step;
			view.render();
			view.view.attach(this.listCont);
			console.log(step);
			console.log(view.view.elHtml);
			this.viewList.push(view.view);
			view.view.elHtml.onclick = () => {
				this.deselectAll();
				view.view.select();
				this.crStep = view.step;
			}
		}

		console.groupEnd();
	}

	editClick(): void {
		//TODO:
		this.crStep;
	}

	deleteClick(): void {
		//TODO:
	}

	addClick(): void {
		console.log('TestPage: add new step click');

		this.detach();
		App.inst.session.crTest.tempStep = new StepObj();

		this.selectStep = new SelectStep();
		this.selectStep.attach(App.inst.cont);
		this.selectStep.mulai();
	}

	public get closeTbl(): HTMLButtonElement {
		return this.getEl('button.close') as HTMLButtonElement;
	}

	// public get editMode(): boolean {
	// 	return this._editMode;
	// }

	// public set editMode(value: boolean) {
	// 	this._editMode = value;
	// }	

	public get judul(): HTMLSpanElement {
		return this._judul;
	}
	public set judul(value: HTMLSpanElement) {
		this._judul = value;
	}

}

//TODO: gabungin sama stepitem, rename jadi listitem
class StepItem {
	private _step: StepObj = null;
	private _view: StepItemView = null;

	constructor() {
		this._view = new StepItemView();
	}

	getStepNameFromCmd(str: string): StepStructure {
		for (let i: number = 0; i < stepTypeListDesc.length; i++) {
			if (str == stepTypeListDesc[i].type) return stepTypeListDesc[i];
		}

		return null;
	}

	render(): void {
		let stepType: StepStructure;
		stepType = this.getStepNameFromCmd(this._step.cmd);
		this.view.desc.innerText = stepType.desc;
		this.view.target = this._step.elName;

		if (StepType.OPEN_BROWSER == this._step.cmd) {
			this.view.target = this._step.data[0];
		}
	}

	public set step(value: StepObj) {
		this._step = value;
		this.render();
	}
	public get view(): StepItemView {
		return this._view;
	}

}

class StepItemView extends BaseComponent {
	//TODO: select
	select(): void {
		console.log('select');
		this.deselect();
		this.elHtml.classList.add('selected');
	}

	deselect(): void {
		this.elHtml.classList.remove('selected');
	}

	constructor() {
		super();
		this._template = `
			<div class='step-type list-item'>
				<p class='desc'></p>
				<p class='target'><span class='el'></span></p>
			</div>
		`;
		this.build();
	}

	public set target(value: string) {
		this.getEl('p.target span.el').innerText = value;
	}

	public get desc(): HTMLParagraphElement {
		return (this.getEl('p.desc') as HTMLParagraphElement)

	}
}