import { IStepObj } from "../StepObj.js";
import { StepStructure, stepTypeListDesc } from "./SelectStep.js";
import { App } from "../App.js";
import { BaseComponent } from "../BaseComponent.js";

export class StepListItem  extends BaseComponent {
	private _step: IStepObj = null;
	// private _view: StepListItemView = null;

	constructor() {
		super();
		this._template = `
			<div class='step-type list-item'>
				<p class='desc'></p>
				<p class='target'><span class='el'></span></p>
			</div>
		`;

		this.build();
		// this._view = new StepListItemView();
	}

	mulai():void {

	}

	select(): void {
		this.deselect();
		this.elHtml.classList.add('selected');
	}

	deselect(): void {
		this.elHtml.classList.remove('selected');
	}	

	getStepNameFromCmd(cmd: string): StepStructure {
		for (let i: number = 0; i < stepTypeListDesc.length; i++) {
			if (cmd == stepTypeListDesc[i].type) return stepTypeListDesc[i];
		}

		console.log("cmd " + cmd);
		throw new Error();
	}

	render(): void {
		let stepType: StepStructure;
		stepType = this.getStepNameFromCmd(this._step.cmd);
		this.desc.innerText = stepType.desc;
		this.target = App.inst.test.stepGetName(this._step);
	}

	public set step(value: IStepObj) {
		this._step = value;
		this.render();
	}

	public get step():IStepObj {
		return this._step;
	}


	public set target(value: string) {
		this.getEl('p.target span.el').innerText = value;
	}

	public get desc(): HTMLParagraphElement {
		return (this.getEl('p.desc') as HTMLParagraphElement)

	}		

	// public get view(): StepListItemView {
	// 	return this._view;
	// }

}

/*
export class StepListItemView extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='step-type list-item'>
				<p class='desc'></p>
				<p class='target'><span class='el'></span></p>
			</div>
		`;
		this.build();
	}

	select(): void {
		this.deselect();
		this.elHtml.classList.add('selected');
	}

	deselect(): void {
		this.elHtml.classList.remove('selected');
	}	

	public set target(value: string) {
		this.getEl('p.target span.el').innerText = value;
	}

	public get desc(): HTMLParagraphElement {
		return (this.getEl('p.desc') as HTMLParagraphElement)

	}
}
*/