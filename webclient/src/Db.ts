import { TestObj, ITestObj } from "./Test";
import { StepObj } from "./Step";

export class StepManager {
	toObj(): void {

	}

	fromObj(): void {

	}
}

export class TestManager {
	private _tests: Array<TestObj> = [];	//TODO: dihapus karena sudah ambil dari storage
	private _nama: string = 'test';

	testIdAda(idx: number): boolean {
		for (let i: number = 0; i < this._tests.length; i++) {
			if (this._tests[i].id == idx) return false;
		}

		return true;
	}

	getById(id: number): TestObj {
		let test: TestObj;
		let tests: TestObj[] = this.loadFromStorage();

		tests.forEach((item: TestObj) => {
			if (item.id == id) {
				test = item;
			}
		});

		return test;
	}

	toObj(obj: TestObj): ITestObj {
		return {
			nama: obj.nama,
			id: obj.id,
			steps: obj.steps,
			steps2: [],
			tempStep: null
		}
	}

	fromObj2(obj: ITestObj): TestObj {
		let hasil: TestObj = new TestObj();
		hasil.id = obj.id;
		hasil.nama = obj.nama;

		obj.steps.forEach((item: StepObj) => {
			hasil.steps.push(item);
		});
		return hasil;
	}

	//depecreated
	// fromObj(obj: any): TestObj {
	// 	let test: TestObj = new TestObj();
	// 	test.id = obj.id;
	// 	test.nama = obj.nama;

	// 	return test;
	// }

	idCreate(): number {
		let idx: number = this._tests.length;

		while (true) {
			if (this.testIdAda(idx)) return idx;
			idx++;
		}
	}

	insert(test: TestObj): void {
		let tests: TestObj[] = this.loadFromStorage();

		test.id = this.idCreate();
		tests.push(test);
		this.saveToStorage(tests);
	}

	//TODO:
	update(id: number, testSrc: TestObj): void {
		// let test: TestObj;
		let tests: TestObj[] = this.loadFromStorage();

		for (let i: number = 0; i < tests.length; i++) {
			if (tests[i].id == id) {
				tests[i] = testSrc;
			}
		}

		this.saveToStorage(tests);

		// tests.forEach((item:TestObj) => {
		// 	if (item.id == id) {
		// 		item`
		// 	}
		// })

		// test = this.getById(id);
		// test.nama = testSrc.nama;

		// while (test.steps.length > 0) {
		// 	test.steps.pop();
		// }

		// testSrc.steps.forEach((item: StepObj) => {
		// 	test.steps.push(item);
		// });

		// this.saveToStorage(tests);
	}

	delete(test: TestObj): void {
		let tests: TestObj[] = this.loadFromStorage();
		for (let i: number = tests.length - 1; i >= 0; i--) {
			if (tests[i].id == test.id) {
				tests.splice(i, 1);
			}
		}
		this.saveToStorage(tests);
	}

	// toJsonData(): string {
	// 	return JSON.stringify(this._tests);
	// }

	toJsonData2(tests: TestObj[]): string {
		let list: ITestObj[] = [];

		tests.forEach((item: TestObj) => {
			list.push(this.toObj(item));
		});

		return JSON.stringify(list);
	}

	loadFromStorage(): TestObj[] {
		let db: Storage = window.localStorage;
		let dataStr: string = '';
		let dataObj: any[];

		while (this._tests.length > 0) {
			this._tests.pop();
		}

		if (db) {
			dataStr = db.getItem(this._nama);
			if (dataStr) {
				dataObj = JSON.parse(dataStr);
				dataObj.forEach((item: ITestObj) => {
					this._tests.push(this.fromObj2(item));
				})
			}
		}
		else {
			console.warn('save failed');
		}

		return this._tests;
	}

	stepDelete(step: StepObj, test: TestObj): void {
		for (let i: number = 0; i < test.steps.length; i++) {
			let item: StepObj = test.steps[i];
			if (item == step) {
				test.steps.splice(i, 1);
			}

		}
	}

	saveToStorage(tests: TestObj[]): void {
		let db: Storage = window.localStorage;
		if (db) {
			db.setItem(this._nama, this.toJsonData2(tests));
		}
		else {
			console.warn('save failed');
		}
	}

	public get tests(): Array<TestObj> {
		return this._tests;
	}

	public get count(): number {
		return this._tests.length;
	}

}