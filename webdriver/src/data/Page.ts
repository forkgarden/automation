import {page as page1} from "./AgentPage";
import {page as supervisorPage1} from "./SupervisorPage";
import {page as adminPage1} from "./AdminPage";

export namespace page {
    export const homePage = {
        btnLogout: { name: "logout", css: "button[id$=btnLogout].text-in-center[name=gtxReserved_noJavaScript_actionString]" },
        dialogConfirmLogout: {
            btnComfirm: { name: "confirm logout", css: "button[id$=btnConfirm]" },
            btnCancel: { name: "", css: "" }
        },
        btnTabWorkList:{}
    }

    export const logoutPage = {
        btnLoginAgain: {name : "relogin", xpath: "//button[contains(text(), 'Login again')][1]"}
    }

    export const loginPage = {
        inputUserName: { name: "user name", css: "div input[type=text]" },
        inputPassword: { name: "user password", css: "div input[type=password]" },
        //btnLogin: { name: "user name", xpath: "//input[@type='submit'][1]" },
        btnLogin: { name: "user name", xpath: "//button[@type='submit'][1]" },
    }

    export const agentPage = page1.agentPage;
    export const supervisorPage = supervisorPage1.supervisorPage;
    export const adminPage = adminPage1.adminPage;
}

export namespace Url {
    export const Ad = "http://localhost:8280/GTConnect/UnifiedAcceptor/FrameworkDesktop.Main?killSession";
    export const AdMahameruQA = "http://qa-mahameru-gdpr-single.glalab.local/GTConnect/UnifiedAcceptor/FrameworkDesktop.Main?killSession";
    export const ChatHomePage = "http://qatest.glalab.local/GTConnect/UnifiedAcceptor/FrameworkDesktop.Main";
    export const ChatWindow = "";
    export const WebLogicAdmin = "";
    export const SocialAdmin = '';
}


