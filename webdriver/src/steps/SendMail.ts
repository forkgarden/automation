import { BaseStep } from "../main/BaseStep";

export class SendMail extends BaseStep {

    run() {
       //write your steps here, you can start it with
       //this.flow.browserOpen().maximize().navigate(Url.Ad)
       this.flow.browserOpen().navigate("http://10.80.31.112/squirrelmail/src/webmail.php")
       .click("//a[contains(text(),'Go to the login page')]")
       
       .write("//input[@type='text']","fajar")
       .write("//input[@type='password']", "fajar")
       .click("//input[@type='submit']")

       .comment("click compose button")
       .switchTo(1)
       .click("//a[contains(text(),'Compose')]")
       
       .write("//input[@type='text'][@name='send_to']","inbound@jakartavm3-mail.com")
       .write("//input[@type='text'][@name='subject']","subject")
       .write("//textarea[@name='body']", "body email")
       .click("//td[@align='right']//input[@type='submit'][@name='send'][@value='Send']")
       .delay(1000 * 5)

       .click("//a[contains(text(),'Sign Out')]")
       .error();
    }
}
