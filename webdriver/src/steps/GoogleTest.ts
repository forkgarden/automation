import { BaseStep } from "../main/BaseStep"

export class GoogleTest extends BaseStep {

	run() {
		//write your steps here, you can start it with
		//this.flow.browserOpen().maximize().navigate(Url.Ad)
		this.flow
			.browserOpen()
			.navigate("http://www.google.com")
			.write("input.gLFyf.gsfi", "automation for dummies")
			.sendKey("input.gLFyf.gsfi")
	}
}
