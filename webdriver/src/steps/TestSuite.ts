import { BaseStep } from "../main/BaseStep";
import { SampleStep } from "./SampleStep";
import { StepWithReusable } from "./StepWithReusable";

export class TestSuite extends BaseStep {
    run() {
        this.flow
        .runOtherTest(new SampleStep())
        .comment("next command")
        .runOtherTest(new StepWithReusable())
        .quit();
    }

} 