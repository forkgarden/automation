import { page } from "../data/Page";
import { Url } from "../data/Page";
import { BaseStep } from "../main/BaseStep";

export class SampleStep extends BaseStep {
    run() {
        console.log("login em");
        this.flow
            .browserOpen() 
            .navigate(Url.Ad)
            .maximize()
            .comment("Login to Em")
            .write(page.loginPage.inputUserName, "whitemail")
            .write(page.loginPage.inputPassword, "whitemail")
            .click(page.loginPage.btnLogin)

            .comment("Identy Costumer as Joe Blogs")
            .click(page.agentPage.contextMenu.btnIdentifyCustomer)
            .write(page.agentPage.IdentifyCustomer.inputFirstName, "j")
            .write(page.agentPage.IdentifyCustomer.inputLastName, "b")
            .click(page.agentPage.IdentifyCustomer.btnSearch)
            .click(page.agentPage.IdentifyCustomer.listSearchResult.firstRow)
            .click(page.agentPage.IdentifyCustomer.btnSelect)

            .comment("WarpUp costumer joe blogs")
            .delay(5000)
            .click(page.agentPage.contextMenu.btnWarpUp)
            .click(page.agentPage.WarpUp.optReasonCode.completed)
            .write(page.agentPage.WarpUp.textAreaNote, "warp up note")
            .click(page.agentPage.WarpUp.btnConfirm)

            .comment("Log out from EM")
            .click(page.homePage.btnLogout)
            .click(page.homePage.dialogConfirmLogout.btnComfirm)
            .checkElementLocated(page.logoutPage.btnLoginAgain)
            .quit()
    }

}