import { page } from "../data/Page";
import { Url } from "../data/Page";
import { BaseStep } from "../main/BaseStep"
// import { createPublicKey } from "crypto";

export class Login extends BaseStep {

	run() {
		//write your steps here, you can start it with
		//this.flow.browserOpen().maximize().navigate(Url.Ad)
		this.flow.browserOpen("firefox").maximize().navigate(Url.AdMahameruQA)
			.comment("Login to EM")
			.write(page.loginPage.inputUserName, "ccadam")
			.write(page.loginPage.inputPassword, "ccadam")
			.click(page.loginPage.btnLogin)

			.comment("Identify Customer")
			.delay(5000)
			.click(page.agentPage.contextMenu.btnIdentifyCustomer)
			.write(page.agentPage.IdentifyCustomer.inputFirstName, "j")
			.write(page.agentPage.IdentifyCustomer.inputLastName, "b")
			.click(page.agentPage.IdentifyCustomer.btnSearch)
			.click(page.agentPage.IdentifyCustomer.listSearchResult.firstRow)
			.click(page.agentPage.IdentifyCustomer.btnSelect)

			.comment("Warp Up")
			.delay(5000)
			.click(page.agentPage.contextMenu.btnWarpUp)
			.click(page.agentPage.WarpUp.optReasonCode.completed)
			.write(page.agentPage.WarpUp.textAreaNote, "warp up note")
			.click(page.agentPage.WarpUp.btnConfirm)

			.comment("Logout")
			.delay(5000)
			.hideLoading()
			.click(page.homePage.btnLogout)
			.click(page.homePage.dialogConfirmLogout.btnComfirm)
			.checkElementLocated(page.logoutPage.btnLoginAgain)
			.quit()
			.error();
	}
}
