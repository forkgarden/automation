import { Url } from "../data/Page";
import { BaseStep } from "../main/BaseStep";
import { RLogin } from "../reusables/RLogin";
import { RIdentifyCustomer } from "../reusables/RIdentifyCustomer";
import { RWarpUp } from "../reusables/RWarpUp";
import { RLogout } from "../reusables/RLogout";

export class StepWithReusable extends BaseStep {
    run() {
        console.log("step with template");
        this.flow
            .browserOpen()
            .navigate(Url.Ad).maximize()
            .combine(new RLogin(), ["whitemail", "whitemail"])
            .combine(new RIdentifyCustomer())
            .combine(new RWarpUp())
            .combine(new RLogout())
            .quit()
    }

}