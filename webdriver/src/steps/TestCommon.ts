import { page } from "../data/Page";
import { Url } from "../data/Page";
import { BaseStep } from "../main/BaseStep"

export class TestCommon extends BaseStep {

    run() {
       this.testDoubleBrowser();
    }

    testDoubleBrowser():void {
        this.flow.browserOpen("firefox").navigate("http://www.google.com")
        .browserOpen("firefox").navigate("http://www.detik.com")
        .delay(5 * 1000)
        .browserSwitch(0).quit()
        .delaySec(5)
        .browserSwitch(1).quit(); 
    }
}
