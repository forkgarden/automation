import { page } from "../data/Page";
import { Url } from "../data/Page";
import { BaseStep } from "../main/BaseStep"
import { KillSession2 } from "./KillSession2";

export class TestJira extends BaseStep {

    run() {
        //write your steps here, you can start it with
        //this.flow.browserOpen().maximize().navigate(Url.Ad)

        this.testJira();

        // this.flow.runOtherTest(new KillSession2())

    }

    testJira(): void {
        this.flow.browserOpen('firefox').maximize().navigate(Url.AdMahameruQA);
        this.flow.comment("login as cccadmin");
        this.login("cccadmin", "cccadmin");

        this.flow.comment("manage desktop")
            .click("//a[contains(.,'Manage Desktop')][1]")     //contextMenu -> agentChat
            .click("(//button[contains(@id, 'Panel')])[3]")
            .click("(((//table[@Summary='List of Profile Types'])[2]/tbody/tr)[2]/td)[1]")
            .click("//button[contains(@id, '_editBtn')]")
            .click("//a[contains(@id, '_tbhListstag1')]")
            .click("//button[contains(@id, '_btnAdd')][contains(., 'Add...')]")
            .click("//li[@rel='root'][@name='organisat_bank'][@level='1']/ins")
            .click("//a[contains(., 'Kana Bank')][@title = 'Kana Bank']")
            .click("//button[contains(@id, '_btnAdd')][contains(., '>')]")
            .click("(//button[contains(@id, '_btnConfirm')])[2]")
            .click("(//button[contains(@id, '_btnConfirm')])[1]")

            .delay(5000)
            .comment("reselect profile, do it twice for work around with stale errors")
            .click("(((//table[@Summary='List of Profile Types'])[2]/tbody/tr)[2]/td)[1]")
            .error()
            .delay(5000)
            .click("(((//table[@Summary='List of Profile Types'])[2]/tbody/tr)[2]/td)[1]")  //TODO: use text content
            .error()
            .click("//button[contains(@id, '_editBtn')]")
            .click("//a[contains(@id, '_tbhListstag1')]")
            .click("((//table[@summary = 'Tags'])[2]//tbody/tr/td)[2]")
            .comment("remove tag")
            .click("(//button[contains(@id, '_btnRemove')])[2]")
            .comment("click confirm")
            .click("(//button[contains(@id, '_btnConfirm')])")

            .comment("reselect first profile")
            .click("(((//table[@Summary='List of Profile Types'])[2]/tbody/tr)[2]/td)[1]")
            .error()
            .delay(5000)
            .click("(((//table[@Summary='List of Profile Types'])[2]/tbody/tr)[2]/td)[1]")
            .error()
            .delay(5000)
            .comment("")
            .click("//button[contains(@id, '_editBtn')]")
            .click("//a[contains(@id, '_tbhListstag1')]")
            .delay(10 * 1000)
            .comment("check that the record is empty")  //TODO: should check based on text, because there could more than one entries
            //check element not exists after 1 minute
            .checkElementLocated("//td/div/a[@class = 'tabbable'][contains(., 'No records found.')]")
            .error()
            // this.logout()
            // .quit()
 
    }
}
