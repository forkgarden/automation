import { page } from "../data/Page";
import { Url } from "../data/Page";
import { BaseStep } from "../main/BaseStep"
import { RLogin } from "../reusables/RLogin";
import { RIdentifyCustomer } from "../reusables/RIdentifyCustomer";
import { RWarpUp } from "../reusables/RWarpUp";
import { RLogout } from "../reusables/RLogout";

export class Reusable extends BaseStep {

    run() {
       //write your steps here, you can start it with
       //this.flow.browserOpen().maximize().navigate(Url.Ad)
       this.flow.browserOpen().navigate(Url.Ad)
       .combine(new RLogin())
       .combine(new RIdentifyCustomer())
       .combine(new RWarpUp())
       .combine(new RLogout())
       .quit();
    }
}
