import { page } from "../data/Page";
import { Url } from "../data/Page";
import { BaseStep } from "../main/BaseStep"

export class KillSession2 extends BaseStep {

    run() {
       //write your steps here, you can start it with
       //this.flow.browserOpen().maximize().navigate(Url.Ad)

       this.flow.browserOpen("firefox").maximize().navigate(Url.AdMahameruQA)
       .login("cccadmin", "cccadmin")
       .quit()

       .comment("re loggin")
       .browserOpen("firefox").maximize().navigate(Url.AdMahameruQA)
       .login("cccadmin", "cccadmin")
       .click(page.agentPage.contextMenu.btnIdentifyCustomer)
       .quit()

       .comment("re login 2")
       .browserOpen("firefox").maximize().navigate(Url.AdMahameruQA)
       .login("cccadmin", "cccadmin")
       .click(page.agentPage.contextMenu.btnIdentifyCustomer)

    }
}
