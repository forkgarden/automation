import { Flow } from "./Flow";
import { page } from "../data/Page";
import { FlowData, StepType } from "./JSONKons";

export class BaseStep {
	protected flow: Flow;

	constructor() {
		this.flow = new Flow();
	}

	runFromData(data: Array<FlowData>): void {
		console.log('runt from data');
		console.log(data);
		for (let i: number = 0; i < data.length; i++) {
			let type: string = data[i]._cmd;
			let item: FlowData = data[i];

			item._data = item._data || [];

			if (StepType.BROWSER_SWITCH == type) {
				this.flow.browserSwitch(item._data[0]);
			}
			else if (StepType.CLICK == type) {
				this.flow.click(item._el);
			}
			else if (StepType.COMBINE == type) {
				this.flow.combine(item.reusable, item._data);
			}
			else if (StepType.COMMENT == type) {
				this.flow.comment(data[i]._data[0]);
			}
			else if (StepType.DELAY == type) {
				this.flow.delay(item._data[0]);
			}
			else if (StepType.DELAY_MIN == type) {
				this.flow.delayMin(item._data[0]);
			}
			else if (StepType.DELAY_SEC == type) {
				this.flow.delaySec(item._data[0]);
			}
			else if (StepType.ERROR == type) {
				this.flow.error();
			}
			else if (StepType.MAXIMIZE == type) {
				this.flow.maximize();
			}
			else if (StepType.NAVIGATE == type) {
				this.flow.navigate(item._data[0]);
			}
			else if (StepType.OPEN_BROWSER == type) {
				this.flow.browserOpen();
			}
			else if (StepType.QUIT == type) {
				this.flow.quit();
			}
			else if (StepType.RUNJS == type) {
				this.flow.runJs(item._data[0]);
			}
			else if (StepType.SEND_KEY == type) {
				this.flow.sendKey(item._el, item._data[0])
			}
			else if (StepType.SWTICH_TO_FRAME == type) {
				this.flow.switchTo(item._data[0]);
			}
			else if (StepType.VALIDATE_ELEMENT_ENABLE == type) {
				this.flow.checkElementEnable(item._el);
			}
			else if (StepType.VALIDATE_ELEMENT_LOCATED == type) {
				this.flow.checkElementLocated(item._el);
			}
			else if (StepType.VALIDATE_ELEMENT_VISIBLE == type) {
				this.flow.checkElementVisible(item._el);
			}
			else if (StepType.WRITE == type) {
				this.flow.write(item._el, item._data[0]);
			}
			else {
				console.log("type unregistered " + type);
				throw new Error("type " + type);
			}
		}
		this.flow.error();

		//TODO: debug
		this.flow.delay(5000);
		this.flow.quit();
	}

	getPromise(): Promise<any> {
		return this.flow.getPromise();
	}

	run(): void {

	}

	login(userName: string = "cccagent", password: string = "cccagent"): void {
		this.flow
			.comment("Login to EM")
			.write(page.loginPage.inputUserName, userName)
			.write(page.loginPage.inputPassword, password)
			.click(page.loginPage.btnLogin)
			.delaySec(10)
	}

	logout(): void {
		this.flow
			.comment("Logout")
			.delay(5000)
			.hideLoading()
			.click(page.homePage.btnLogout)
			.click(page.homePage.dialogConfirmLogout.btnComfirm)
			.checkElementLocated(page.logoutPage.btnLoginAgain)
			.quit();
	}

	warpUp(): void {
		this.flow
			.comment("Warp Up Customer")
			.delay(5000)
			.click(page.agentPage.contextMenu.btnWarpUp)
			.click(page.agentPage.WarpUp.optReasonCode.completed)
			.write(page.agentPage.WarpUp.textAreaNote, "note")
			.click(page.agentPage.WarpUp.btnConfirm)
	}
}