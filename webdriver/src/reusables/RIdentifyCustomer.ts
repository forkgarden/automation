import { Flow } from "../main/Flow";
import { BaseReusable } from "../main/BaseReusable";
import { page } from "../data/Page";

export class RIdentifyCustomer extends BaseReusable {
    run(flow: Flow, opts:Array<any> = []): void {

        if (!opts[0]) opts[0] = "j";
        if (!opts[1]) opts[1] = "b";

        flow
            .comment("Identify customer as Joe Blogs")
            .delay(5000)
            .click(page.agentPage.contextMenu.btnIdentifyCustomer)
            .write(page.agentPage.IdentifyCustomer.inputFirstName, opts[0])
            .write(page.agentPage.IdentifyCustomer.inputLastName, opts[1])
            .click(page.agentPage.IdentifyCustomer.btnSearch)
            .delay(5000)
            .click(page.agentPage.IdentifyCustomer.listSearchResult.firstRow)
            .click(page.agentPage.IdentifyCustomer.btnSelect)
    }
}