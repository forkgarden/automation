import { Flow } from "../main/Flow";
import { BaseReusable } from "../main/BaseReusable";
import { page } from "../data/Page";

export class RLogin extends BaseReusable {
    run(flow: Flow, opts:Array<any>=[]): void {

        if (!opts[0]) opts[0] = "ccadam";
        if (!opts[1]) opts[1] = "ccadam";

        flow.comment("Login to Em")
            .write(page.loginPage.inputUserName, opts[0])
            .write(page.loginPage.inputPassword, opts[1])
            .click(page.loginPage.btnLogin);
    }
}