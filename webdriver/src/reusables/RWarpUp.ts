import { Flow } from "../main/Flow";
import { BaseReusable } from "../main/BaseReusable";
import { page } from "../data/Page";

export class RWarpUp extends BaseReusable {
    run(flow: Flow, opts:Array<any> = []): void {
        if (!opts[0]) opts[0] = "warp up note";
        flow
            .comment("Warp Up Customer")
            .delay(5000)
            .click(page.agentPage.contextMenu.btnWarpUp)
            .click(page.agentPage.WarpUp.optReasonCode.completed)
            .write(page.agentPage.WarpUp.textAreaNote, opts[0])
            .click(page.agentPage.WarpUp.btnConfirm)
    }
}