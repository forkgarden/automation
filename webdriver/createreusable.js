var fs = require('fs');

const args = process.argv;

var content = ''+
'import { Flow } from "../main/Flow";\n'+
'import { BaseTemplate } from "../main/BaseTemplate";\n'+
'import { page } from "../data/Page"\n'+
'\n'+
'export class ' + args[2] + ' extends BaseTemplate {\n'+
'\n'+
'    run(flow: Flow, args:Array<any> = []): void {\n'+
'       //write your template here, you can start it with\n'+
'       //flow.comment("Login to Em")\n'+
'    }\n'+
'}\n';

fs.writeFile("src/reusables/" + args[2] + ".ts", content, () => {});